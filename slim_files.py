#!/bin/env python

from __future__ import print_function

import os, sys, argparse
import time
from datetime import timedelta
import glob
import json
import errno
import ROOT as r
r.gROOT.SetBatch(True)
r.PyConfig.IgnoreCommandLineOptions = True

filelist_dir = "/data/uclhc/uci/user/dantrim/n0307val/susynt-read/filelists/"

class FakeSample :
    def __init__(self, name = "", filelist_loc = "", file_location = "") :
        self.name = name
        self.filelist_loc = filelist_loc
        self.file_location = file_location
        self.sample_dsids = []

def get_dsids_from_lists(filelists) :

    dsids = []
    is_data = False

    for flist in filelists :
        fname = flist.strip().split('/')[-1]
        look_for = ""
        if 'mc16_13TeV' in fname :
            look_for = "mc16_13TeV."
        elif 'data15_13TeV' in fname :
            look_for = 'data15_13TeV.00'
            is_data = True
        elif 'data16_13TeV' in fname :
            look_for = 'data16_13TeV.00'
            is_data = True
        elif 'data17_13TeV' in fname :
            look_for = 'data17_13TeV.00'
            is_data = True
        elif 'data18_13TeV' in fname :
            look_for = 'data18_13TeV.00'
            is_data = True

        dsid = fname[fname.find(look_for) + len(look_for) : fname.find(look_for) + len(look_for) + 6]
        dsids.append(dsid)

    return dsids, is_data

def load_and_check_that_samples_are_ok(faketrees) :

    all_ok = True

    for ftree in faketrees :
        filelist_loc = ftree.filelist_loc
        filelists = glob.glob("{}/*.txt".format(filelist_loc))
        n_lists = len(filelists)
        if n_lists == 0 :
            print("WARNING Did not find any filelists for sample {} in expected location: {}".format(ftree.name, filelist_loc))
            all_ok = False
        dsids_from_lists, sample_is_data = get_dsids_from_lists(filelists)
        ftree.sample_dsids = dsids_from_lists
        ftree.is_data = sample_is_data

        file_location = ftree.file_location
        glob_str = ""
        campaign = ""
        if "mc16a" in ftree.name :
            campaign = "mc16a"
        elif "mc16d" in ftree.name :
            campaign = "mc16d"
        elif "mc16e" in ftree.name :
            campaign = "mc16e"

        if sample_is_data :
            glob_str = "{}/CENTRAL*.root".format(file_location)
        else :
            glob_str = "{}/CENTRAL*{}.root".format(file_location, campaign)
        all_files = glob.glob(glob_str)

        n_expected = len(dsids_from_lists)
        sample_files = []
        for f in all_files :
            fname = f.split("/")[-1]
            for dsid in dsids_from_lists :
                if str(dsid) not in fname : continue
                sample_files.append(f)
        n_for_sample = len(sample_files)
        if n_expected != n_for_sample :
            print("WARNING Did not find expected number (={}) of files for sample {}, we got {} files instead".format(n_expected, ftree.name, n_for_sample))
            if n_for_sample == 0 :
                all_ok = False
        ftree.sample_files = sample_files

    return all_ok

def get_slim_tcuts() :

    trigger = "( ( year == 2015 && trig_tight_2015 == 1 ) || ( year == 2016 && trig_tight_2016 == 1 ) || ( year == 2017 && trig_tight_2017rand == 1 ) || ( year == 2018 &&  trig_tight_2018 == 1 ))"
    sr = "nBJets>=2 && mbb>80 && mbb<160 && mll>20 && mll<60"

    mbb_no_dhh = "nBJets>=2 && mll>20 && mll<60 && mbb>100 && mbb<140"
    cr_top = "nBJets>=2 && mll>20 && mll<60 && (mbb<100 || mbb>140)"
    cr_z = "nBJets>=2 && mll>81.2 && mll<101.2 && mbb>100 && mbb<140"
    vr_z = "nBJets>=2 && ( (mll>71.2 && mll<81.2) || (mll>101.2 && mll<115) ) && mbb>100 && mbb<140"

    or_sel = "( %s ) || ( %s ) || ( %s ) || ( %s )" % (mbb_no_dhh, cr_top, cr_z, vr_z)
    return or_sel
    

def merge_ftree(ftree, args) :

    output_ftree_common = "CENTRAL_ftree_{}".format(ftree.name)
    print(80 * "=")
    print("INFO Merging FakeTree for sample {}".format(ftree.name))
    start_time = time.time()

    var_filename = "wwbb_vars.txt"
    relevant_vars = []
    with open(var_filename, "r") as var_file :
        for line in var_file :
            if line.strip().startswith("#") : continue
            relevant_vars.append(line.strip())

    output_tree_name = output_ftree_common
    if args.suffix != "" :
        output_tree_name += "_{}".format(args.suffix)
    output_tree_name += ".root"

    merge_chain = r.TChain("superNt")
    tree_name = "superNt"

    for f in ftree.sample_files :
        merge_chain.AddFile(f, 0, tree_name)
    merge_chain.SetBranchStatus("*",0)
    for rel_var in relevant_vars :
        merge_chain.SetBranchStatus(rel_var, 1)
    slim_cuts = get_slim_tcuts()
    merge_chain = merge_chain.CopyTree(slim_cuts)
    merge_chain.SetName("superNt")
    merge_chain.SetTitle("superNt")

    output_file = r.TFile(output_tree_name, "RECREATE")
    output_file.cd()
    merge_chain.Write()
    output_file.Close()

    end_time = time.time()
    delta = end_time - start_time
    print("INFO     >> Finished {}, took {} ".format(ftree.name, str(timedelta(seconds = delta)) ))


def make_slimmed_files(config, args) :


    all_samples = [s for s in config["mc_samples"]]
    all_samples.extend( [s for s in config["data_sample"]])
    all_samples_names = [s["name"] for s in all_samples]

    selected_samples = []
    if args.select_sample :
        for s in args.select_sample :
            for asample in all_samples :
                if asample["name"] == s :
                    selected_samples.append(asample)
    else :
        selected_samples = all_samples

    global filelist_dir
    
    ftrees = []
    for sample in selected_samples :
        name = sample["name"]
        filelists = sample["lists"]
        file_locations = sample["filedirs"]
        for ifilelist, filelist in enumerate(filelists) :
            full_filelist_location = "{}/{}/".format(filelist_dir, filelist)
            file_location = file_locations[ifilelist]
            campaign = filelist.strip().split("_")[-1]
            if args.campaign :
                if campaign not in args.campaign :
                    continue
            ftree = FakeSample("{}_{}".format(name, campaign), full_filelist_location, file_location)
            ftrees.append(ftree)

    if not load_and_check_that_samples_are_ok(ftrees) :
        sys.exit()

    for ftree in ftrees :
        merge_ftree(ftree, args)

def main() :
    parser = argparse.ArgumentParser(description = "Build slimmed tree files")
    parser.add_argument("-j", "--json", required = True,
        help = "Provide input JSON file descriptor"
    )
    parser.add_argument("-s", "--suffix", default = "",
        help = "Provide a suffix to append to any output"
    )
    parser.add_argument("--select-sample", nargs = "+",
        help = "Select specific sample(s)"
    )
    parser.add_argument("--campaign", nargs = "+",
        help = "Select a specific mc campaign"
    )
    args = parser.parse_args()

    with open(args.json) as jf :
        config = json.load(jf)

    make_slimmed_files(config, args)

if __name__ == "__main__" :
    main()
