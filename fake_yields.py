#!/bin/env python

from __future__ import print_function

import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True
r.gROOT.SetBatch(True)
r.gStyle.SetOptStat(False)

import os, sys
import glob
import argparse
import json
from math import sqrt

filelist_dir = "/data/uclhc/uci/user/dantrim/n0307val/susynt-read/filelists/"

class Region :
    def __init__(self, region_config = {}) :
        self.config = region_config
        self.name = ""
        self.cut = ""
        self.load()
    def load(self) :
        cfg = self.config
        self.name = cfg["name"]
        self.cut = cfg["cut"]

class SimpleSample :
    def __init__(self, sample_config = {}) :
        self.config = sample_config
        self.name = None
        self.tree = None
        self.filename = ""
        self.load()

    def load(self) :
        cfg = self.config
        self.name = cfg["name"]
        self.filename = cfg["filename"]
        c = r.TChain("superNt")
        c.Add(self.filename)
        print("Sample {} added {} entries (from file {})".format(self.name, c.GetEntries(), self.filename))
        self.tree = c

class Sample :
    def __init__(self, list_dir = "", sample_config = {}) :
        self.config = sample_config
        self.list_dir = list_dir
        self.name = None
        self.tree = None
        self.files = []
        self.load()

    def load(self) :

        global filelist_dir

        cfg = self.config

        sample_files = []

        # get the name
        self.name = cfg["name"]
        #if "zjets" not in self.name :
        #if True :
        if "data" not in self.name and "zjets" not in self.name :
            for ifilelist, filelist in enumerate(cfg["lists"]) :
    
                # get the DSIDs from the lists
                dsids_for_list = []
                full_filelist_dir = "{}/{}".format(filelist_dir, filelist)
                txt_files = glob.glob("{}/*.txt".format(full_filelist_dir))
                for itf, tf in enumerate(txt_files) :
                    tfshort = tf.strip().split("/")[-1]
                    dsid = tfshort.split("13TeV.")[1].split(".")[0]
                    if "data" in cfg["name"] :
                        dsid = dsid[2:]
                    dsids_for_list.append(dsid)
                print(" > Found {} DSIDs for sample {}".format(len(dsids_for_list), cfg["name"]))
    
                # now get the files for the associated list
                filedir = cfg["filedirs"][ifilelist]
                all_files = glob.glob("{}/CENTRAL*.root".format(filedir))
                files_for_list = []
                for idsid, dsid in enumerate(dsids_for_list) :
                    for iaf, af in enumerate(all_files) :
                        afshort = af.strip().split("/")[-1]
                        if dsid in afshort :
                            files_for_list.append(af)
                            break
                print(" > Found {} files for sample {}".format(len(dsids_for_list), cfg["name"]))
    
                n_expected = len(dsids_for_list)
                n_found = len(files_for_list)
                if n_expected != n_found :
                    print(" > WARNING Sample {} Did not find expected number of samples (expected: {}, got: {}) {}".format(self.name, n_expected, n_found, filelist))
    
                for f in files_for_list :
                    sample_files.append(f)
            print(" > Found a total of {} files across all lists for sample {}".format(len(sample_files), cfg["name"]))
    
            self.files = sample_files
            c = r.TChain("superNt")
            added_a = False
            added_d = False
            added_e = False
            for f in sample_files :
              #  if "mc16a" in f and added_a : continue
              #  if "mc16d" in f and added_d : continue
              #  if "mc16e" in f and added_e : continue
                print("ADDING FILE {} for {}".format(f, self.name))
                c.Add(f)
              #  if "mc16a" in f : added_a = True
              #  if "mc16d" in f : added_d = True
              #  if "mc16e" in f : added_e = True
            print(" > Loaded {} entries for sample {}".format(c.GetEntries(), cfg["name"]))
            self.tree = c
        elif "data" in self.name :
            data_file = "/data/uclhc/uci/user/dantrim/n0307val/wwbb_fake_estimate/CENTRAL_ftree_data15161718_data15161718.root"
            c = r.TChain("superNt")
            c.Add(data_file)
            self.tree = c
            print(" > Loaded {} entries for sample {}".format(c.GetEntries(), cfg["name"]))
        elif "zjets" in self.name :
            #zjets_file = "/data/uclhc/uci/user/dantrim/ntuples/n0307/i_apr10_fakes/mc/zjets.root"
            #zjets_file = "/data/uclhc/uci/user/dantrim/ntuples/n0307/i_apr10_fakes/mc/mc16a/zjets_mc16a/zjets_mc16a.root"
            zjets_file = "/data/uclhc/uci/user/dantrim/n0307val/wwbb_fake_estimate/CENTRAL_ftree_zjets_mc16e.root"
            #zjets_file = "/data/uclhc/uci/user/dantrim/n0307val/wwbb_fake_estimate/CENTRAL_ftree_zjets_mc16ade.root"
            c = r.TChain("superNt")
            c.Add(zjets_file)
            self.tree = c

def get_samples(sample_configs, args) :

    print("Loading {} sample configs".format(len(sample_configs)))

    samples = []
    for sc in sample_configs :
        if args.simple :
            s = SimpleSample(sc)
        else :
            s = Sample(list_dir = "", sample_config = sc)
        samples.append(s)
    return samples

def get_regions(region_configs) :

    print("Loading {} region configs".format(len(region_configs)))
    regions = []
    for rc in region_configs :
        region = Region(rc)
        regions.append(region)
    return regions

def fake_selections_dict() :

    fake_selections = { "heavy_flavor_e" : "is_prompt==0 && is_NP_heavy_flavor==1 && (isEE==1 || (isEM==1 && lead_is_fake==1) || (isME==1 && sublead_is_fake==1))",
                        "conversion_e" : "is_prompt==0 && is_NP_conversion==1 && (isEE==1 || (isEM==1 && lead_is_fake==1) || (isME==1 && sublead_is_fake==1))",
                        "other_e" : "is_prompt==0 && is_NP_other==1 && (isEE==1 || (isEM==1 && lead_is_fake==1) || (isME==1 && sublead_is_fake==1))",
                        "heavy_flavor_m" : "is_prompt==0 && is_NP_heavy_flavor==1 && (isMM==1 || (isEM==1 && sublead_is_fake==1) || (isME==1 && lead_is_fake==1))",
                        "other_m" : "is_prompt==0 && is_NP_other==1 && (isMM==1 || (isEM==1 && sublead_is_fake==1) || (isME==1 && lead_is_fake==1))"
    }
    #fake_selections = { "non_prompt" : "is_prompt==0", "prompt" : "is_prompt==1" }
    return fake_selections


def get_fake_yield_in_region(mc_samples, data_sample, region, args) :

    fake_yields = {}
    for fake_type in fake_selections_dict() :
        fake_yields[fake_type] = [0.0, 0.0]

    cut = region.cut
    weight_str = "eventweightNoPRW_multi * 140.48"

    print("Yields for %s: %s" % (region.name, region.cut))
    trigger = "((year==2015 && trig_tight_2015==1) || (year==2016 && trig_tight_2016==1) || (year==2017 && trig_tight_2017rand==1) || (year==2018 && trig_tight_2018==1))"

    for imc, sample in enumerate(mc_samples) :
        print(" > %s..." % sample.name)
        for ift, fake_type in enumerate(fake_yields) :
            fake_selection = fake_selections_dict()[fake_type]
            full_cut = "({} && {} && {}) * {}".format(trigger, cut, fake_selection, weight_str)

            h = r.TH1F("h_{}_{}_{}".format(imc, ift, region.name), "", 4, 0, 4)
            h.Sumw2()

            cmd = "isMC>>{}".format(h.GetName())
            sample.tree.Draw(cmd, full_cut, "goff")
            err = r.Double(0.0)
            integral = h.IntegralAndError(0,-1,err)
            print("Sample %s : Fake type %s = %.4f +/- %.4f" % (sample.name, fake_type, integral, err))

            fake_yields[fake_type][0] += integral
            fake_yields[fake_type][1] += err * err

    for fake_type in fake_yields :
        fake_yields[fake_type][1] = sqrt(fake_yields[fake_type][1])

    total_fake_yield = 0.0
    total_fake_yield_err = 0.0


    print(40 * "- ")
    print("Fake totals for region {}".format(region.name))
    for fake_type in fake_yields :
        print(" %s : %.4f +/- %.4f" % (fake_type, fake_yields[fake_type][0], fake_yields[fake_type][1]))
        total_fake_yield += fake_yields[fake_type][0]
        total_fake_yield_err += fake_yields[fake_type][1] * fake_yields[fake_type][1]
    total_fake_yield_err = sqrt(total_fake_yield_err)
    print(" -> Total fake MC for region %s : %.4f +/- %.4f" % (region.name, total_fake_yield, total_fake_yield_err))

    if region.name == "ss_out" :
        print(55 * "--")
        print("Getting SS data yields in region %s" % (region.name))
        full_cut = "({} && {})".format(trigger, cut)
        h_data = r.TH1F("h_data_{}".format(region.name), "", 4, 0, 4)
        h_data.Sumw2()

        cmd = "isMC>>{}".format(h_data.GetName())
        data_sample.tree.Draw(cmd, full_cut, "goff")
        err = r.Double(0.0)
        integral = h_data.IntegralAndError(0,-1,err)
        print("SS data yield: %.4f +/- %.4f" % (integral, err))
            

def get_fake_yields(mc_samples, data_sample, regions, args) :

    n_reg = len(regions)
    for ireg, reg in enumerate(regions) :
        print(65 * "=")
        print("Getting yields for region: {}".format(reg.name))
        get_fake_yield_in_region(mc_samples, data_sample, reg, args)

def get_fake_yields_in_sr(mc_samples, data_sample, args) :

    print(65 * "=")
    print("Getting fake yields in SR...")

    ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.45 && isSF==1"
    os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.45 && isSF==1"
    #ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.55 && isDF==1"
    #os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.55 && isDF==1"

    ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh<4.5 && isDF==1"
    os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh<4.5 && isDF==1"

    ss_region_cut = "isSS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh<0"
    os_region_cut = "isOS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh<0"

    #ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>140 && NN_d_hh<4.5 && isSF==1"
    #os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>140 && NN_d_hh<4.5 && isSF==1"

    #ss_region_cut = "isSS==1 && nBJets>=2 && ( (mll>71.2 && mll<81.2) || (mll>101.2 && mll<115) ) && mbb>110 && mbb<140 && NN_d_hh<0"
    #os_region_cut = "isOS==1 && nBJets>=2 && ( (mll>71.2 && mll<81.2) || (mll>101.2 && mll<115) ) && mbb>110 && mbb<140 && NN_d_hh<0"

    # SF SR
    #ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.45 && isDF==1"
    #os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.45 && isDF==1"

    sr_cuts = { "ss_sr" : ss_region_cut,
                "os_sr" : os_region_cut
            }

    weight_str = "eventweightNoPRW_multi * 140.48"
    trigger = "((year==2015 && trig_tight_2015==1) || (year==2016 && trig_tight_2016==1) || (year==2017 && trig_tight_2017rand==1) || (year==2018 && trig_tight_2018==1))"

    prompt_ss_yield = 0.0
    prompt_ss_yield_err = 0.0
    for imc, mc in enumerate(mc_samples) :
        #if "ttbar" in mc.name : continue

        prompt_sel = "is_prompt==1"
        full_cut = "({} && {} && {}) * {}".format(trigger, sr_cuts["ss_sr"], prompt_sel, weight_str)
        h = r.TH1F("h_prompt_ss_{}".format(imc), "", 4, 0, 4)
        h.Sumw2()
        cmd = "isMC>>{}".format(h.GetName())
        mc.tree.Draw(cmd, full_cut, "goff")
        #if "zjets" not in mc.name :
        #    mc.tree.Draw(cmd, full_cut, "goff")
        #else :
        #    #zjets_file = "/data/uclhc/uci/user/dantrim/ntuples/n0307/i_apr10_fakes/mc/zjets.root"
        #    zjets_file = "/data/uclhc/uci/user/dantrim/ntuples/n0307/i_apr10_fakes/mc/zjets_mc16a/zjets_mc16a.root"
        #    rfile = r.TFile.Open(zjets_file)
        #    tree = rfile.Get("superNt")
        #    tree.Draw(cmd, full_cut, "goff")
        err = r.Double(0.0)
        integral = h.IntegralAndError(0,-1,err)
        prompt_ss_yield += integral
        prompt_ss_yield_err += err * err
        print("  Prompt MC for %s in SS region: %.4f +/- %.4f" % (mc.name, integral, err))
    prompt_ss_yield_err = sqrt(prompt_ss_yield_err)
    print("  => Total prompt MC in SS region: %.4f +/- %.4f" % (prompt_ss_yield, prompt_ss_yield_err))

    data_cut = "(%s && %s) * 1" % (trigger, ss_region_cut)
    h_data = r.TH1F("h_data_ss", "", 4, 0, 4)
    h_data.Sumw2()
    cmd = "isMC>>%s" % h_data.GetName()
    data_sample.tree.Draw(cmd, data_cut, "goff")
    data_ss_yield_err = r.Double(0.0)
    data_ss_yield = h_data.IntegralAndError(0,-1,data_ss_yield_err)
    print("  => Total data in SS region      : %.4f +/- %.4f" % ( data_ss_yield, data_ss_yield_err ))

    data_ss_fake = data_ss_yield - prompt_ss_yield
    data_ss_fake_up = data_ss_yield - (prompt_ss_yield - 0.5 * prompt_ss_yield)
    data_ss_fake_dn = (data_ss_yield - (prompt_ss_yield + 0.5 * prompt_ss_yield))
    print("  => Data SS fake yield           : %.4f (+%.4f, -%.4f)" % (data_ss_fake, data_ss_fake_up - data_ss_fake, data_ss_fake - data_ss_fake_dn))

    ##
    ## TF
    ##
    #nom_tf = 2.56
    #tf_stat_uncert = 0.34
    nom_tf = 2.672 # sr
    nom_tf = 2.672 # sr
    nom_tf = 2.522 # cr-top
    #nom_tf = 2.337 # vr-top
    #nom_tf = 2.481 # cr-z
    #nom_tf = 2.748 # vr-z
    nom_tf = 2.165
   # nom_tf = 2.130
    nom_tf = 1.962
    nom_tf = 2.655
    tf_stat_uncert = 0.05
    tf_sys_uncert = 0.2
    tf_uncert = nom_tf * sqrt( (tf_sys_uncert * nom_tf) * (tf_sys_uncert * nom_tf)) # + tf_stat_uncert * tf_stat_uncert )
    ss_os_tf = { "nom" : nom_tf, "up" : nom_tf + tf_uncert, "down" : nom_tf - tf_uncert }
    e_dhh = 0.00167 # sr-sf
    e_dhh = 0.0015 # sr-df
    e_dhh = 0.0018 # cr-top
    #e_dhh = 0.007 # vr-top
    #e_dhh = 0.0253 # cr-z
    #e_dhh = 0.0097 # vr-z
    e_dhh = 0.00144
    #e_dhh = 0.00220
    e_dhh = 0.00582
    e_dhh = 0.03466

    n_fake_sr_med = ss_os_tf["nom"] * data_ss_fake * e_dhh
    n_fake_sr_up = ss_os_tf["up"] * data_ss_fake * e_dhh
    n_fake_sr_dn = ss_os_tf["down"] * data_ss_fake * e_dhh

    n_fake_sr_sub_up = ss_os_tf["nom"] * data_ss_fake_up * e_dhh
    n_fake_sr_sub_dn = ss_os_tf["nom"] * data_ss_fake_dn * e_dhh

    print(55 * "= ")
    print("N fake in SR (SS->OS      variations) : %.3f (+%.3f, -%.3f)" % (n_fake_sr_med, n_fake_sr_up - n_fake_sr_med, n_fake_sr_med - n_fake_sr_dn))
    print("N fake in SR (Subtraction variations) : %.3f (+%.3f, -%.3f)" % (n_fake_sr_med, n_fake_sr_sub_up - n_fake_sr_med, n_fake_sr_med - n_fake_sr_sub_dn))

def add_overflow_to_lastbin(hist) :
    '''
    Given an input histogram, add the overflow
    to the last visible bin
    '''
    # find the last bin
    ilast = hist.GetXaxis().GetNbins()

    # read in the values
    lastBinValue = hist.GetBinContent(ilast)
    lastBinError = hist.GetBinError(ilast)
    overFlowValue = hist.GetBinContent(ilast+1)
    overFlowError = hist.GetBinError(ilast+1)

    # set the values
    hist.SetBinContent(ilast+1, 0)
    hist.SetBinError(ilast+1, 0)
    hist.SetBinContent(ilast, lastBinValue+overFlowValue)
    hist.SetBinError(ilast, sqrt(lastBinError*lastBinError + overFlowError*overFlowError))

def variables_dict() :

    v = {}
    v["abs(dphi_ll)"] = [[10, 0, 3.2], "|#Delta #phi_{ll}|"]
    v["l0_pt"] = [[8, 20, 180], "Leading Lepton p_{T} [GeV]"]
    v["l1_pt"] = [[5, 10, 60], "Sub-leading Lepton p_{T} [GeV]"]
    v["mll"] = [[5, 20, 60], "Dilepton Invariant Mass [GeV]"]
    v["mll"] = [[11, 80, 102], "Dilepton Invariant Mass [GeV]"]
    #v["NN_d_hh"] = [[10, -6, 4], "d_{hh}"]
    #v["NN_d_hh"] = [[16, -8, 8], "d_{hh}"] # cr_top, vr_top
    v["NN_d_hh"] = [[16, -10, 6], "d_{hh}"] # cr_z
    #v["NN_d_hh"] = [[12, -4, 8], "d_{hh}"] # sr
    return v

def make_fake_plots(mc_samples, data_sample, regions, args) :

    #var_to_plot = "abs(dphi_ll)"
    ##var_to_plot = "mll"
    #var_to_plot = "l0_pt"
    ##var_to_plot = "NN_d_hh"
    #varname_hist = var_to_plot.replace("abs(","").replace(")","")
    #var_bounds = variables_dict()[var_to_plot]
    #x_bounds = var_bounds[0]
    #x_label = var_bounds[1]
    #n_bins = x_bounds[0]
    #x_lo = x_bounds[1]
    #x_hi = x_bounds[2]

    vars_to_plot = ["l0_pt", "l1_pt", "mll", "NN_d_hh"]

    #region_to_plot = "sr_sf_ss"
    #region_to_plot = "os_out"
    selected_region = None
#    for region in regions :
#        if region.name == region_to_plot :
#            selected_region = region
    for var in vars_to_plot :
        var_to_plot = var
        varname_hist = var_to_plot.replace("abs(","").replace(")","")
        var_bounds = variables_dict()[var_to_plot]
        x_bounds = var_bounds[0]
        x_label = var_bounds[1]
        n_bins = x_bounds[0]
        x_lo = x_bounds[1]
        x_hi = x_bounds[2]

        for region in regions :
            selected_region = region
            nice_region_names = { "cr_z_ss" : "CR-Z (SS, no d_{hh})",
                                    "cr_z_os" : "CR-Z (OS, no d_{hh})",
                                    "cr_top_ss" : "CR-Top (SS, no d_{hh})",
                                    "cr_top_os" : "CR-Top (OS, no d_{hh})",
                                    "sr_df_ss" : "SR-DF (SS, no d_{hh})",
                                    "sr_df_os" : "SR-DF (OS, no d_{hh})",
                                    "sr_sf_ss" : "SR-SF (SS, no d_{hh})",
                                    "sr_sf_os" : "SR-SF (OS, no d_{hh})" }

            print(85 * "=")
            print("Plotting {} for region {} ({})".format(var_to_plot, selected_region.name, selected_region.cut))

            fake_sources = ["heavy_flavor_e", "conversion_e", "other_e", "heavy_flavor_m", "other_m"]
            nice_names = { "prompt" : "Prompt", "data" : "Data",
                        "heavy_flavor_e" : "HF e",
                        "conversion_e" : "#gamma-conv.",
                        "other_e" : "Other e",
                        "heavy_flavor_m" : "HF #mu",
                        "other_m" : "Other #mu"
            }
            colors = { "prompt" : "#1478fb",
                        "heavy_flavor_e" : "#fd7722",
                        "conversion_e" : "#fffd38",
                        "other_e" : "#650965",
                        "heavy_flavor_m" : "#fc28fc",
                        "other_m" : "#2dfffe"
            }

            mc_histos = []
            h_names = {}

            trigger = "((year==2015 && trig_tight_2015==1) || (year==2016 && trig_tight_2016==1) || (year==2017 && trig_tight_2017rand==1) || (year==2018 && trig_tight_2018==1))"
            weight_str = "eventweightNoPRW_multi * 140.48"

            ##
            ## get the prompt MC histogram
            ##

            h_prompt = r.TH1F("h_prompt_{}_{}".format(selected_region.name, varname_hist), ";{};Events".format(x_label), n_bins, x_lo, x_hi)
            h_prompt.SetFillColor(r.TColor.GetColor(colors["prompt"]))
            h_prompt.SetFillStyle(1001)
            h_prompt.Sumw2()
            for imc, mc in enumerate(mc_samples) :
                prompt_selection = "is_prompt==1"
                h = r.TH1F("h_prompt_{}_{}_{}".format(imc, selected_region.name, varname_hist), ";{};Events".format(x_label), n_bins, x_lo, x_hi)
                #h.SetFillColor(r.TColor.GetColor(colors["prompt"]))
                #h.SetFillStyle(1001)
                #h.Sumw2()
                #full_cut = "({} && {}) * {}".format(selected_region.cut, prompt_selection, weight_str)
                full_cut = "({} && {} && {}) * {}".format(trigger, selected_region.cut, prompt_selection, weight_str)

                cmd = "{}>>+{}".format(var_to_plot, h_prompt.GetName())
                mc.tree.Draw(cmd, full_cut, "goff")
                #h_prompt.Add(h)
            add_overflow_to_lastbin(h_prompt)
            err = r.Double(0.0)
            integral_prompt = h_prompt.IntegralAndError(0,-1,err)
            print("Total Prompt: %.4f +/- %.4f" % (integral_prompt, err))
            mc_histos.append(h_prompt)

            ##
            ## get the fake histograms
            ##
            total_np = 0.0
            total_np_err = 0.0
            for isource, fake_source in enumerate(fake_sources) :
                h_source = r.TH1F("h_{}_{}_{}".format(fake_source, selected_region.name, varname_hist), ";{};Events".format(x_label), n_bins, x_lo, x_hi)
                h_source.Sumw2()
                h_source.SetFillColor(r.TColor.GetColor(colors[fake_source]))
                h_source.SetFillStyle(1001)
                h_names[h_source.GetName()] = nice_names[fake_source]

                fake_sel = fake_selections_dict()[fake_source]
                #full_cut = "({} && {}) * {}".format(selected_region.cut, fake_sel, weight_str)
                full_cut = "({} && {} && {}) * {}".format(trigger, selected_region.cut, fake_sel, weight_str)

                for imc, mc in enumerate(mc_samples) :
                    #h = r.TH1F("h_{}_{}_{}_{}".format(fake_source, imc, selected_region.name, varname_hist), ";{};Events".format(x_label), n_bins, x_lo, x_hi)
                    #h.SetFillColor(r.TColor.GetColor(colors[fake_source]))
                    #h.SetFillStyle(1001)
                    #h.Sumw2()

                    cmd = "{}>>+{}".format(var_to_plot, h_source.GetName())
                    mc.tree.Draw(cmd, full_cut, "goff") 
                    #h_source.Add(h)
                add_overflow_to_lastbin(h_source)
                err = r.Double(0.0)
                integral = h_source.IntegralAndError(0,-1,err)
                total_np += integral
                total_np_err += err * err
                print("  -> %s: %.4f +/- %.4f" % (fake_source, integral, err))
                mc_histos.append(h_source)
            print("Total Non Prompt : %.3f +/- %.3f" % (total_np, sqrt(total_np_err)))

            ##
            ## get the same-sign data
            ##

            h_data = r.TH1F("h_data_{}".format(varname_hist), ";{};Events".format(x_label), n_bins, x_lo, x_hi)
            h_data.SetMarkerStyle(20)
            h_data.SetMarkerColor(r.kBlack)
            h_data.SetMarkerSize(2 * h_data.GetMarkerSize())
            h_data.Sumw2()

            full_cut = "({} && {})".format(trigger, selected_region.cut)
            cmd = "{}>>{}".format(var_to_plot, h_data.GetName())
            data_sample.tree.Draw(cmd, full_cut, "goff")
            add_overflow_to_lastbin(h_data)
            err = r.Double(0.0)
            integral = h_data.IntegralAndError(0,-1,err)
            #h_data.Add(h_prompt, -1.0)
            print("Total Data SS: %.4f +/- %.4f" % (integral, err))

            ##
            ## start drawing
            ##

            c = r.TCanvas("c_{}".format(varname_hist), "", 700, 800)
            c.SetTicks(1,1)
            c.SetLeftMargin(1.15 * c.GetLeftMargin())
            c.cd()

            prompt_h = mc_histos[0]
            fake_hs = mc_histos[1:]

            leg = r.TLegend(0.7, 0.65, 0.88, 0.88)
            leg.SetBorderSize(0)
            leg.AddEntry(h_data, "Data", "p")
            leg.AddEntry(mc_histos[0], "Prompt", "f")
            for ih, h in enumerate(fake_hs) :
                leg.AddEntry(h, h_names[h.GetName()], "f")

            stack = r.THStack("stack_{}".format(varname_hist), ";{};Events".format(x_label))
            stack.SetTitle("")
            for h in fake_hs[::-1] :
                stack.Add(h)
            max_y = stack.GetMaximum()
            max_data = h_data.GetMaximum()
            if max_data > max_y : max_y = max_data
            stack.SetMaximum(1.25 * max_y)
            h_data.SetMaximum(1.25 * max_y)
            stack.SetMinimum(0)
            h_data.SetMinimum(0)
            stack.Add(prompt_h)

            stack.Draw("hist")
            c.Update()
            r.gPad.Modified()
            stack.GetXaxis().SetTitle(x_label)
            stack.GetYaxis().SetTitle("Events")

            text = r.TLatex() 
            text.SetTextFont(42)
            text.SetNDC()
            text.DrawLatex(0.15, 0.84, nice_region_names[selected_region.name])
            c.Update()

            leg.Draw()
            c.Update()

            h_data.Draw("p0 e same")
            c.Update()

            ##
            ## save
            ##
            outname = "fake_val_plots_ttbar_semi/fake_val_{}_{}.pdf".format(selected_region.name, varname_hist)
            print("Saving: {}".format(os.path.abspath(outname)))
            c.SaveAs(outname)

def get_os_ss_ratio_sys(mc_samples, regions, args) :

    var_to_plot = "NN_d_hh"
    var_bounds = variables_dict()[var_to_plot]
    x_bounds = var_bounds[0]
    x_label = var_bounds[1]
    n_bins = x_bounds[0]
    x_lo = x_bounds[1]
    x_hi = x_bounds[2]

    fake_sources = ["heavy_flavor_e", "conversion_e", "other_e", "heavy_flavor_m", "other_m"]
    region_names = ["ss_out", "os_out"]

    histos = {}
    trigger = "((year==2015 && trig_tight_2015==1) || (year==2016 && trig_tight_2016==1) || (year==2017 && trig_tight_2017rand==1) || (year==2018 && trig_tight_2018==1))"
    weight_str = "eventweightNoPRW_multi * 140.48"

    ss_name = ""
    os_name = ""
    for region in regions :
        if "ss" in region.name : ss_name = region.name
        if "os" in region.name : os_name = region.name

    fake_selections = { "heavy_flavor_e" : "is_prompt==0 && is_NP_heavy_flavor==1 && (isEE==1 || (isEM==1 && lead_is_fake==1) || (isME==1 && sublead_is_fake==1))",
                        "conversion_e" : "is_prompt==0 && is_NP_conversion==1 && (isEE==1 || (isEM==1 && lead_is_fake==1) || (isME==1 && sublead_is_fake==1))",
                        "other_e" : "is_prompt==0 && is_NP_other==1 && (isEE==1 || (isEM==1 && lead_is_fake==1) || (isME==1 && sublead_is_fake==1))",
                        "heavy_flavor_m" : "is_prompt==0 && is_NP_heavy_flavor==1 && (isMM==1 || (isEM==1 && sublead_is_fake==1) || (isME==1 && lead_is_fake==1))",
                        "other_m" : "is_prompt==0 && is_NP_other==1 && (isMM==1 || (isEM==1 && sublead_is_fake==1) || (isME==1 && lead_is_fake==1))"
    }

    variations = []
    up_vars = []
    dn_vars = []
    for fake_sel in fake_selections :
        nom = fake_sel
        up = "%s_UP" % fake_sel
        dn = "%s_DN" % fake_sel
        variations.append([nom, up, dn])

    histos = {} # { region : { sys :histo } }
    for region in regions :

        print("Getting nominal histo for region {}".format(region.name))
        h_nom = r.TH1F("h_nom_os_ss_{}".format(region.name), ";d_{hh};", n_bins, x_lo, x_hi)
        h_nom.Sumw2()
        fake_sel_nom = "is_prompt==0 && %s" % fake_selections["conversion_e"]
        full_cut = "({} && {} && {}) * {}".format(trigger, region.cut, fake_sel_nom, weight_str)
        for imc, mc in enumerate(mc_samples) :
            if imc > 0 : break
            cmd = "{}>>+{}".format(var_to_plot, h_nom.GetName())
            mc.tree.Draw(cmd,full_cut,"goff")
        histos[region.name] = {}
        histos[region.name]["nominal"] = h_nom

        print("Getting sys varied histos for region {}".format(region.name))
        for ivar, variation in enumerate(variations) :

            if "conversion" not in variation[0] : continue
            print("  -> {}".format(variation[0]))

            up_vars.append(variation[1])
            dn_vars.append(variation[2])

            h_up = r.TH1F("h_up_os_ss_{}_{}".format(region.name, variation[1]), ";d_{hh};", n_bins, x_lo, x_hi)
            h_up.Sumw2()

            h_dn = r.TH1F("h_dn_os_ss_{}_{}".format(region.name, variation[2]), ";d_{hh};", n_bins, x_lo, x_hi)
            h_dn.Sumw2()

            for isource, fake_source in enumerate(fake_sources) :
                if fake_source in variation[0] : continue # don't add here the one for which we are varying
                fake_sel = fake_selections[fake_source]
                full_cut = "({} && {} && {}) * {}".format(trigger, region.cut, fake_sel, weight_str)
                for imc, mc in enumerate(mc_samples) :

                    if imc > 0 : break

                    cmd = "{}>>+{}".format(var_to_plot, h_up.GetName())
                    mc.tree.Draw(cmd,full_cut,"goff")

                    cmd = "{}>>+{}".format(var_to_plot, h_dn.GetName())
                    mc.tree.Draw(cmd,full_cut,"goff")

            for imc, mc in enumerate(mc_samples) :
                if imc > 0 : break

                cmd = "{}>>+{}".format(var_to_plot, h_up.GetName())
                full_cut = "({} && {} && {}) * {} * 1.20".format(trigger, region.cut, fake_selections[variation[0]], weight_str)
                mc.tree.Draw(cmd,full_cut,"goff")

                cmd = "{}>>+{}".format(var_to_plot, h_dn.GetName())
                full_cut = "({} && {} && {}) * {} * 0.8".format(trigger, region.cut, fake_selections[variation[0]], weight_str)
                mc.tree.Draw(cmd,full_cut,"goff")

            histos[region.name][variation[1]] = h_up
            histos[region.name][variation[2]] = h_dn

    ##
    ## start drawing
    ##

    c = r.TCanvas("c_os_ss_sys", "", 700, 800)
    c.SetTicks(1,1)
    c.SetLeftMargin(1.1 * c.GetLeftMargin())
    c.cd()

    h_nom_f = histos[os_name]["nominal"]
    h_nom_den = histos[ss_name]["nominal"]
    h_nom_f.Divide(h_nom_den)
    h_nom_f.SetMarkerStyle(20)
    h_nom_f.SetMarkerSize(1.65 * h_nom_f.GetMarkerSize())
    h_nom_f.SetLineColor(r.kBlack)
    h_nom_f.SetLineWidth(2)
    h_nom_f.SetMarkerColor(r.kBlack)
    h_nom_f.SetMaximum(5)
    h_nom_f.SetMinimum(5)

    h_nom_f.GetYaxis().SetTitle("SS#rightarrowOS Fake Transfer Factor")
    h_nom_f.Draw("p0 e")
    c.Update()

    ##
    ## draw all UP variations
    ##
    h_variation_f_up = []
    h_variation_f_dn = []
    for up_var in up_vars :
        h_up_f = histos[os_name][up_var]
        h_up_den = histos[ss_name][up_var]
        h_up_f.Divide(h_up_den)
        h_up_f.SetLineColor(r.kRed)
        h_up_f.SetLineWidth(2)
        h_up_f.SetLineStyle(1)
        h_up_f.SetMaximum(5)
        h_up_f.SetMinimum(0)
        h_variation_f_up.append(h_up_f)
        #h_up_f.Draw("hist same")
        #c.Update()
    ##
    ## draw all DN variations
    ##
    for dn_var in dn_vars :
        h_dn_f = histos[os_name][dn_var]
        h_dn_den = histos[ss_name][dn_var]
        h_dn_f.Divide(h_dn_den)
        h_dn_f.SetLineColor(r.kGreen)
        h_dn_f.SetLineWidth(2)
        h_dn_f.SetLineStyle(1)
        h_dn_f.SetMaximum(5)
        h_dn_f.SetMinimum(0)
        h_variation_f_dn.append(h_dn_f)
        #h_dn_f.Draw("hist same")
        #c.Update()

    delta_up = []
    delta_dn = []
    n_bins = histos[os_name]["nominal"].GetNbinsX()
    maxima = []
    minima = []
    for ibin in range(n_bins) :
        bin_no = ibin+1
        maxy = 1e-9
        miny = 1e9
        for ih, h in enumerate(h_variation_f_up) :
            yval = h.GetBinContent(bin_no)
            if yval > maxy : maxy = yval
            if yval < miny : miny = yval
        for ih, h in enumerate(h_variation_f_dn) :
            yval = h.GetBinContent(bin_no)
            if yval > maxy : maxy = yval
            if yval < miny : miny = yval
        maxima.append(maxy)
        minima.append(miny)

    delta_nom_up = []
    delta_nom_dn = []
    for ibin in range(n_bins) :
        bin_no = ibin+1
        nom_val = h_nom_f.GetBinContent(bin_no)
        max_val = maxima[ibin]
        min_val = minima[ibin]

        if nom_val == 0 :
            delta_up = 0
            delta_dn = 0
        else :
            delta_up = abs(max_val - nom_val) / nom_val
            delta_dn = abs(nom_val - min_val) / nom_val
        delta_nom_up.append(delta_up)
        delta_nom_dn.append(delta_dn)

    h_up = r.TH1F("h_os_ss_envelope_up", ";d_{hh};", n_bins, x_lo, x_hi)
    h_up.SetLineColor(r.kBlue)
    h_dn = r.TH1F("h_os_ss_envelope_dn", ";d_{hh};", n_bins, x_lo, x_hi)
    h_dn.SetLineColor(r.kRed)
    for idx, max_val in enumerate(maxima) :
        min_val = minima[idx]
        nom_val = h_nom_f.GetBinContent(idx+1)
        h_up.SetBinContent(idx+1, nom_val + abs(delta_nom_up[idx]))
        h_dn.SetBinContent(idx+1, nom_val - abs(delta_nom_dn[idx]))
        #h_up.SetBinContent(idx+1, max_val)
        #h_dn.SetBinContent(idx+1, min_val)

    h_up.Draw("hist same")
    c.Update()
    h_dn.Draw("hist same")
    c.Update()

    nice_names = {
        "cr_top" : "CR-Top",
        "cr_z"   : "CR-Z+HF",
        "vr_top" : "VR-Top",
        "vr_z"   : "VR-Z+HF",
        "sr_df"  : "SR-DF",
        "sr_sf"  : "SR-SF",
        "sr_inc" : "SR"
    }

    region_name = ss_name.replace("_ss","").replace("ss_","")
    text = r.TLatex()
    text.SetNDC()
    text.SetTextFont(42)
    text.DrawLatex(0.15, 0.19, "Selection: %s" % nice_names[region_name])
    c.Update()

    ##
    ## save
    ##

    outname = "fake_val_plots/fake_val_os_ss_ratio_SYS_NN_d_hh_%s.pdf" % region_name
    print("Saving: {}".format(os.path.abspath(outname)))
    c.SaveAs(outname)

def get_os_ss_ratio(mc_samples, regions, args) :

    var_to_plot = "NN_d_hh"
    var_bounds = variables_dict()[var_to_plot]
    x_bounds = var_bounds[0]
    x_label = var_bounds[1]
    n_bins = x_bounds[0]
    x_lo = x_bounds[1]
    x_hi = x_bounds[2]

    fake_sources = ["heavy_flavor_e", "conversion_e", "other_e", "heavy_flavor_m", "other_m"]
    region_names = ["ss_out", "os_out"]

    histos = {}
    trigger = "((year==2015 && trig_tight_2015==1) || (year==2016 && trig_tight_2016==1) || (year==2017 && trig_tight_2017rand==1) || (year==2018 && trig_tight_2018==1))"
    weight_str = "eventweightNoPRW_multi * 140.48"

    ss_name = ""
    os_name = ""
    for region in regions :
        if "ss" in region.name : ss_name = region.name
        if "os" in region.name : os_name = region.name
        print("Getting {} : {}".format(region.name, region.cut))

        h = r.TH1F("h_os_ss_{}".format(region.name), ";d_{hh};", n_bins, x_lo, x_hi)
        h.Sumw2()

        for imc, mc in enumerate(mc_samples) :
            fake_cut = "is_prompt==0"
            full_cut = "({} && {} && {}) * {}".format(trigger, region.cut, fake_cut, weight_str)
            cmd = "NN_d_hh>>+{}".format(h.GetName())
            mc.tree.Draw(cmd, full_cut, "goff")

        add_overflow_to_lastbin(h)
        histos[region.name] = h

        if "ss" in region.name or "os" in region.name :
            h_yields = r.TH1F("h_yield_ss_%s" % region.name, "", 4, 0, 4)
            fake_cut = "is_prompt==0"
            full_cut = "({} && {} && {}) * {}".format(trigger, region.cut, fake_cut, weight_str)
            cmd = "isMC>>+{}".format(h_yields.GetName())
            for imc, mc in enumerate(mc_samples) :
                mc.tree.Draw(cmd, full_cut, "goff")
            err = r.Double(0.0)
            integral = h_yields.IntegralAndError(0,-1,err)
            print("SS yield for %s: %.4f +/- %.4f" % (region.name, integral, err))

    ##
    ## start drawing
    ##

    c = r.TCanvas("c_os_ss", "", 700, 800)
    c.SetTicks(1,1)
    c.cd()

    h_num = histos[os_name]
    h_den = histos[ss_name]
    h_num.Divide(h_den)
    h_num.SetMarkerStyle(20)
    h_num.SetMarkerSize(1.65 * h_num.GetMarkerSize())
    h_num.SetLineColor(r.kBlack)
    h_num.SetLineWidth(2)
    h_num.SetMarkerColor(r.kBlack)
#    h_num.SetMaximum(1.1 * h_num.GetMaximum())
    h_num.SetMaximum(5)
    h_num.SetMinimum(0)

    c.SetLeftMargin( 1.1 * c.GetLeftMargin())
    h_num.GetYaxis().SetTitle("SS#rightarrowOS Fake Transfer Factor")

    h_num.Draw("p0 e")
    c.Update()

    ##
    ## text
    ##

    nice_names = {
        "cr_top" : "CR-Top",
        "cr_z"   : "CR-Z+HF",
        "vr_top" : "VR-Top",
        "vr_z"   : "VR-Z+HF",
        "sr_df"  : "SR-DF",
        "sr_sf"  : "SR-SF",
        "sr_inc" : "SR"
    }
    region_name = ss_name.replace("_ss","").replace("ss_","")
    text = r.TLatex()
    text.SetNDC()
    text.SetTextFont(42)
    text.DrawLatex(0.15, 0.19, "Selection: %s" % nice_names[region_name])
    c.Update()


    ##
    ## get spread
    ##

    h_spread = r.TH1F("h_tf_spread", "", 100, 0, -1)
    h_spread.Sumw2()
    vals = []
    for i in range(h_num.GetNbinsX()) :
        bin_no = i+1
        bc = h_num.GetBinContent(bin_no)
        vals.append(bc)
        h_spread.Fill(bc)
    mean_h = h_spread.GetMean()
    var_tf = h_spread.GetStdDev()
    mean_tf = sum(vals) / len(vals)
    var_tf = 0.2 * mean_tf
    print("mean_tf = %.3f" % mean_tf)
    print("mean_h  = %.3f +/- %.3f" % (mean_h, var_tf))

    text.SetTextSize(0.75 * text.GetTextSize())
    text.DrawLatex(0.15, 0.14, "f^{SS#rightarrowOS} = %.3f #pm %.3f" % (mean_tf, var_tf))
    c.Update()


    line = r.TLine(x_lo, mean_tf, x_hi, mean_tf)
    line.SetLineColor(r.kBlue)
    line.SetLineStyle(2)
    line.SetLineWidth(2)
    line.Draw()
    c.Update()

    line_up = r.TLine(x_lo, mean_tf + var_tf, x_hi, mean_tf + var_tf)
    line_up.SetLineColor(r.kBlue)
    line_up.SetLineStyle(3)
    line_up.SetLineWidth(1)
    line_up.Draw()

    line_dn = r.TLine(x_lo, mean_tf - var_tf, x_hi, mean_tf - var_tf)
    line_dn.SetLineColor(r.kBlue)
    line_dn.SetLineStyle(3)
    line_dn.SetLineWidth(1)
    line_dn.Draw()

    c.Update()


    ##
    ## save
    ##
    outname = "fake_val_plots_ttbar_semi/fake_val_os_ss_ratio_NN_d_hh_%s.pdf" % region_name
    print("Saving: {}".format(os.path.abspath(outname)))
    c.SaveAs(outname)

def get_fake_yield_from_sample(sample, tcut) :

    weight_str = "eventweightNoPRW_multi * 140.48"

    trigger = "((year==2015 && trig_tight_2015==1) || (year==2016 && trig_tight_2016==1) || (year==2017 && trig_tight_2017rand==1) || (year==2018 && trig_tight_2018==1))"

    fake_sel = "is_prompt==0"
    full_cut = "({} && {} && {}) * {}".format(trigger, fake_sel, tcut, weight_str)
    h = r.TH1F("h_{}_fakes".format(sample.name), "", 4, 0, 4)
    h.Sumw2()
    cmd = "isMC>>{}".format(h.GetName())
    sample.tree.Draw(cmd, full_cut, "goff")
    err = r.Double(0.0)
    integral = h.IntegralAndError(0,-1,err)
    return integral, err

def get_dhh_efficiency(mc_samples, args) :


    reg_no_mbb_out = ""
    reg_no_mbb_inside = ""

    cuts_no_mbb_out = {
        "cr_top" : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh<4.5 && isDF==1",
        "vr_top" : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>140 && NN_d_hh<4.5 && isSF==1",
        "cr_z"   : "isOS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh<0",
        "vr_z"   : "isOS==1 && nBJets>=2 && ( (mll>71.2 && mll<81.2) || (mll>101.2 && mll<115) ) && mbb>110 && mbb<140 && NN_d_hh<0",
        "sr_df"  : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.55 && isDF==1",
        "sr_sf"  : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5.45 && isSF==1"
    }
    cuts_no_mbb_in = {
        "cr_top" : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh>4.5 && isDF==1",
        "vr_top" : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>140 && NN_d_hh>4.5 && isSF==1",
        "cr_z"   : "isOS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh>0",
        "vr_z"   : "isOS==1 && nBJets>=2 && ( (mll>71.2 && mll<81.2) || (mll>101.2 && mll<115) ) && mbb>110 && mbb<140 && NN_d_hh>0",
        "sr_df"  : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh>5.55 && isDF==1",
        "sr_sf"  : "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh>5.45 && isSF==1"
    }

    for region in args.region :
        print("Getting d_hh cut efficiency for %s" % region)
        if len(region.strip().split("_")) > 2 :
            region = "_".join(region.strip().split("_")[:2])
        cut_out = cuts_no_mbb_out[region]
        cut_in = cuts_no_mbb_in[region]

        total_fake_yield_out = 0.0
        total_fake_yield_out_err = 0.0
        total_fake_yield_in = 0.0
        total_fake_yield_in_err = 0.0

        for imc, mc in enumerate(mc_samples) :
            fake_yield_out, fake_yield_out_err = get_fake_yield_from_sample(mc, cut_out)
            fake_yield_in, fake_yield_in_err = get_fake_yield_from_sample(mc, cut_in)

            total_fake_yield_out += fake_yield_out
            total_fake_yield_out_err += fake_yield_out_err * fake_yield_out_err
            total_fake_yield_in += fake_yield_in
            total_fake_yield_in_err += fake_yield_in_err * fake_yield_in_err

        total_fake_yield_out_err = sqrt(total_fake_yield_out_err)
        total_fake_yield_in_err = sqrt(total_fake_yield_in_err)

        e_dhh = total_fake_yield_in / total_fake_yield_out
        e_dhh_err = e_dhh * sqrt( (total_fake_yield_out_err/total_fake_yield_out) ** 2 + (total_fake_yield_in_err/total_fake_yield_in) ** 2)

        print(" => d_hh cut efficiency for region %s: %.5f +/- %.5f" % (region, e_dhh, e_dhh_err))

def get_fake_shape(mc_samples, data_sample, args) :

    region_name = "Same-sign SR (inverted d_{hh})"
    region_name_simple = "sr_ss_out"
    ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5" 
    os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5"

    region_name = "Opposite-sign SR (inverted d_{hh})"
    region_name_simple = "sr_os_out"
    ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5" 
    os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh<5"

    region_name = "Same-sign CR-Top (inverted d_{hh})"
    region_name_simple = "cr_top_ss_out"
    ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh<4.5 && isDF==1"
    os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh<4.5 && isDF==1"

    region_name = "Opposite-sign CR-Top (inverted d_{hh})"
    region_name_simple = "cr_top_os_out"
    ss_region_cut = "isSS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh<4.5 && isDF==1"
    os_region_cut = "isOS==1 && nBJets>=2 && mll>20 && mll<60 && (mbb<110 || mbb>140) && NN_d_hh<4.5 && isDF==1"

    region_name = "Same-sign CR-Z+HF (inverted d_{hh})"
    region_name_simple = "cr_z_ss_out"
    ss_region_cut = "isSS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh<0"
    os_region_cut = "isOS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh<0"

   # region_name = "Opposite-sign CR-Z+HF (inverted d_{hh})"
   # region_name_simple = "cr_z_os_out"
   # ss_region_cut = "isSS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh<0"
   # os_region_cut = "isOS==1 && nBJets>=2 && mll>81.2 && mll<101.2 && mbb>110 && mbb<140 && NN_d_hh<0"

    trigger = "((year==2015 && trig_tight_2015==1) || (year==2016 && trig_tight_2016==1) || (year==2017 && trig_tight_2017rand==1) || (year==2018 && trig_tight_2018==1))"
    weight_str = "eventweightNoPRW_multi * 140.48"

    var_to_plot = "NN_d_hh"
    x_label = "d_{hh}"
    n_bins = 8
    x_lo = -5
    x_hi = 5
    x_lo = -3
    x_hi = 5

    n_bins = 8
    x_lo = -8
    x_hi = 0

    ##
    ## get same sign data
    ##

    print("Getting SS data...")
    h_data_ss = r.TH1F("h_data_ss", ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
    h_data_ss.Sumw2()
    data_cut = "(%s && %s)" % (trigger, ss_region_cut)
    cmd = "%s>>%s" % (var_to_plot, h_data_ss.GetName())
    data_sample.tree.Draw(cmd, data_cut, "goff")

    ##
    ## get MC prompt
    ##

    print("Getting SS MC prompt...")
    h_mc_prompt_ss = r.TH1F("h_mc_prompt", ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
    h_mc_prompt_ss.Sumw2()
    prompt_ss_cut = "(%s && is_prompt==1 && %s) * %s" % (trigger, ss_region_cut, weight_str)
    cmd = "%s>>+%s" % (var_to_plot, h_mc_prompt_ss.GetName())
    for imc, mc in enumerate(mc_samples) :
        mc.tree.Draw(cmd, prompt_ss_cut, "goff")

    ##
    ## subtract the prompt from teh data ss
    ##
    h_data_ss.Add(h_mc_prompt_ss, -1)
    h_data_ss.Scale(1.0 / h_data_ss.Integral())

    ##
    ## get the fake MC
    ##
    print("Getting non prompt MC...")
    h_mc_non_prompt_ss = r.TH1F("h_mc_non_prompt", ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
    h_mc_non_prompt_ss.Sumw2()
    region_cut = ss_region_cut
    if "os" in region_name_simple :
        region_cut = os_region_cut
    non_prompt_ss_cut = "(%s && is_prompt==0 && %s) * %s" % (trigger, region_cut, weight_str)
    cmd = "%s>>+%s" % (var_to_plot, h_mc_non_prompt_ss.GetName())
    for imc, mc in enumerate(mc_samples) :
        mc.tree.Draw(cmd, non_prompt_ss_cut, "goff")
    h_mc_non_prompt_ss.Scale(1.0 / h_mc_non_prompt_ss.Integral())

    ##
    ## draw
    ##

    c = r.TCanvas("c_fake_shape_%s" % var_to_plot, "", 700, 800)
    c.SetTicks(1,1)
    c.SetLeftMargin(1.15 * c.GetLeftMargin())
    c.cd()


    maxy = h_data_ss.GetMaximum()
    if h_mc_non_prompt_ss.GetMaximum() > maxy : maxy = h_mc_non_prompt_ss.GetMaximum()
    maxy *= 1.25

    h_data_ss.SetMinimum(0)
    h_mc_non_prompt_ss.SetMinimum(0)
    h_data_ss.SetMaximum(maxy)
    h_mc_non_prompt_ss.SetMaximum(maxy)

    h_data_ss.SetLineColor(r.kBlack)
    h_data_ss.SetLineWidth(2)
    h_mc_non_prompt_ss.SetLineColor(r.kRed)
    h_mc_non_prompt_ss.SetLineWidth(2)

    h_data_ss.Draw("hist e")
    c.Update()

    h_mc_non_prompt_ss.Draw("hist e same")
    c.Update()

    leg = r.TLegend(0.6, 0.7, 0.87, 0.8)
    leg.SetBorderSize(0)
    leg.AddEntry(h_data_ss, "DD Fakes", "l")
    leg.AddEntry(h_mc_non_prompt_ss, "MC Fakes", "l")
    leg.Draw()

    text = r.TLatex()
    text.SetNDC()
    text.SetTextFont(42)
    text.SetTextSize(0.65 * text.GetTextSize())
    text.DrawLatex(0.15, 0.85, "Selection: %s" % region_name)
    c.Update()

    ##
    ## save
    ##
    outname = "fake_val_plots/fake_shape_%s_%s.pdf" % (var_to_plot, region_name_simple)
    print("Saving: {}".format(os.path.abspath(outname)))
    c.SaveAs(outname)
    


def main() :

    parser = argparse.ArgumentParser(description = "Get fake yields")
    parser.add_argument("-i", "--input", required = True,
        help = "Provide an input JSON configuration file"
    )
    parser.add_argument("-r", "--region", nargs = "+",
        help = "Select a specific region to get the yields in"
    )
    parser.add_argument("--mconly", default = False, action = "store_true",
        help = "Only get the MC based yields"
    )
    parser.add_argument("--dd", default = False, action = "store_true",
        help = "Extract data-driven fake estimate in the given region"
    )
    parser.add_argument("--simple", default = False, action = "store_true",
        help = "Load SimpleSamples"
    )
    args = parser.parse_args()

    if not os.path.isfile(args.input) :
        print("ERROR Input config file (={}) not found".format(args.input))
        sys.exit()

    with open(args.input, "r") as input_config :
        config_data = json.load(input_config)

    mc_samples = get_samples(config_data["mc_samples"], args)
    data_sample = get_samples(config_data["data_sample"], args)
    regions = get_regions(config_data["selections"])

    if args.region :
        tmp_reg = []
        for region in regions :
            if region.name in args.region :
                tmp_reg.append(region)
        regions = tmp_reg

    #get_fake_yields(mc_samples = mc_samples, data_sample = data_sample[0], regions = regions, args = args)
    #get_fake_yields_in_sr(mc_samples = mc_samples, data_sample = data_sample[0], args = args)
    make_fake_plots(mc_samples, data_sample[0], regions, args)
    #get_os_ss_ratio(mc_samples, regions, args)
    #get_os_ss_ratio_sys(mc_samples, regions, args)
    #get_dhh_efficiency(mc_samples = mc_samples, args = args)
    #get_fake_shape(mc_samples = mc_samples, data_sample = data_sample[0], args = args)

if __name__ == "__main__" :
    main()
